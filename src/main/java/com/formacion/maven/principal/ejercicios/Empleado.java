package com.formacion.maven.principal.ejercicios;

public class Empleado {

//Atributos	
	int id;
	String area;
	double salario;

//Constructor 
	/*
	 * public Empleado(int id, String area,double salario ) {
	 * 
	 * this.id= id; this.area = area; this.salario = salario;
	 * 
	 * }
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	double calcularSalario(int edad, double salario) {

		if (edad > 16) {

			if (edad > 18 && edad < 51) {
				salario = salario * 1.05;
				return salario;
			} else {

				if (edad > 50 && edad < 61) {
					salario = salario * 1.10;
					return salario;

				}

				else {
					if (edad > 60)
						salario = salario * 1.15;
					return salario;
				}

			}

		}
		// Es menor de edad no puede trabajar
		return 0;

	}

}
