package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class AppPersonas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		Persona persona = new Persona();

		// Solicita la informaci�n de los empleados
		System.out.println(" **** Ingresar los datos del empleado ***");

		for (int i = 1; i <= 5; i++) {
			System.out.print("Ingresa el nombre: ");
			persona.setNombre(sc.next());
			System.out.print("Ingresa la edad: ");
			persona.setEdad (sc.nextInt());
			System.out.print("Ingresa el genero: ");
			persona.setGenero(sc.next());
			System.out.print("Ingresa el salario: ");
			double salario1 = sc.nextDouble(); // Variable auxiliar para el salario

			persona.id = i;

			
			if (persona.edad < 16) {
				System.out.println("No tiene edad para trabajar");
				persona.salario = 0;
				System.out.println(persona.toString());
			} else {
				double sueldo = persona.calcularSalario(persona.edad, salario1);
				persona.salario = sueldo;
				System.out.println(persona.toString());
			}
		}

	}

}
