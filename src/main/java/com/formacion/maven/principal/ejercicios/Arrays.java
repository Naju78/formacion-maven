package com.formacion.maven.principal.ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Integer> lista = new ArrayList<Integer>();

		int num;
		int suma = 0;
		int i = 0;

		System.out.println("Ejercicio 5");
		
		Scanner obtenerNumero = new Scanner(System.in);
		
		for ( i = 0; i < 5; i++) {

			System.out.println ("Introduce numero: ");
			num = obtenerNumero.nextInt();
			
			lista.add(num);
		    suma = suma + num;
		    
		}

		//Imprime numero del arreglo
		
		System.out.println ("Imprime Lista ");
		for (i = 0; i < 5; i++) {

			System.out.println("Numero: " + lista.get(i));
			
		}
		
		System.out.println ("Imprime Suma: " + suma);
		
		//Imprime numero del arreglo de forma inversa
		

		for (int j = 4; j >= 0 ; j--) {

			System.out.println("Numero: " + lista.get(j));
			
		}
		
		

	}

}
