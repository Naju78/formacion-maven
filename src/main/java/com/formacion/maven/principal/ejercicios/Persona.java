package com.formacion.maven.principal.ejercicios;


public class Persona extends Empleado
 {

		//Atributos
		String nombre;
		int edad;
		String genero;
		
		//M�todos Getters y Setters
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public int getEdad() {
			return edad;
		}
		public void setEdad(int edad) {
			this.edad = edad;
		}
		public String getGenero() {
			return genero;
		}
		public void setGenero(String genero) {
			this.genero = genero;
		}
		
		
		@Override
		public String toString() {
	      return  "Id: " + id + "Empleado: " +nombre + "  Edad: " + edad +
	    		  "  Genero: " + genero + "  Salario: " + salario;
	   }

}
