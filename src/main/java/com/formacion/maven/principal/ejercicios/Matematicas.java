package com.formacion.maven.principal.ejercicios;

public class Matematicas {
	
	static int suma(int n1, int n2 )
    {   
		int resultado;
		
		resultado = n1 + n2;
		
    	return resultado;
    }

	static int resta(int n1, int n2 )
    {   
		int resultado;
		
		resultado = n1 - n2;
		
    	return resultado;
    }
	
	static int multiplicacion(int n1, int n2 )
    {   
		int resultado;
		
		resultado = n1 * n2;
		
    	return resultado;
    }
	
	static int division(int n1, int n2 )
    {   
		int resultado;
		
		resultado = n1 / n2;
		
    	return resultado;
    }
}
