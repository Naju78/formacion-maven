package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {

		int n;
		double factorial;
		
		Scanner sc = new Scanner(System.in);

		System.out.println("**** CALCULO DE FACTORIAL ****");
		do {
			System.out.print("Introduce un n�mero > 0: ");
			n = sc.nextInt();
		} while (n < 0); // Leer un n�mero entero >= 0

		for (int i = 0; i <= n; i++) { // para cada n�mero desde 1 hasta N
			// se calcula su factorial
			factorial = 1;
			for (int j = 1; j <= i; j++) {
				factorial = factorial * j;
			}
			
			// se muestra el factorial
			System.out.printf("%2d! = %.0f %n", i, factorial);
		}
				

	}

}
