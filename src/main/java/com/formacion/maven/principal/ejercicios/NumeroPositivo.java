package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class NumeroPositivo {

	public static void main(String[] args) {
		
		int num=0;
		
		Scanner sc = new Scanner(System.in);

			System.out.println ("Introduce numero: ");
		
		num = sc.nextInt();
		
		
		if (num > 0)
		{
			System.out.println ("El numero es positivo: " + num);
		}else
		{
			System.out.println ("El numero es negativo: " + num);
		}

		if(num%2==0) {
			System.out.println ("El numero es par: " + num);
		}else
		{
			System.out.println ("El numero es impar: " + num);
		}	 
		

	}

}
