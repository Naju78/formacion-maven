package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class Suma {

	public static void main(String[] args) {

		int num;
		int supar = 0;
		int suimpar = 0; 
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println ("Introduce numero: ");

        num = sc.nextInt();
        
        for(int indice=1;indice<=num;indice++) {
            if(indice%2==0) {
                supar = indice + supar;
            }
            else {
                suimpar = indice + suimpar;
            } 
        }
        
        System.out.println("La suma de los pares es: " + supar);
        System.out.println("La suma de los impares es: " + suimpar);
	}

}
