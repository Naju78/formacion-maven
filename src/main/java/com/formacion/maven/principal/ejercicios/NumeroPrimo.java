package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class NumeroPrimo {

	public static void main(String[] args) {

		Scanner obtenerNumero = new Scanner(System.in);
		int contador,I,num;
		String resp=null;
		
		
		System.out.println("Ejercicio 4 de validar numero primo y preguntar");
		do
		{
		System.out.println ("Introduce numero: ");
		num = obtenerNumero.nextInt();
		
		contador = 0;
		
        for(I = 1; I <= num; I++)
        {
            if((num % I) == 0)
            {
                contador++;
            }
        }
 
        if(contador <= 2)
        {
            System.out.println("El numero es primo");
        }else{
            System.out.println("El numero no es primo");
        }		

        System.out.print("-->Deseas validar otro numero S/N: ");
		resp = obtenerNumero.next();

		}while ((resp.equals("S")));

	}
}
