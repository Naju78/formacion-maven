package com.formacion.maven.principal.ejercicios;

public class NumerosAleatoreos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int numensayo = 0, listo = 0;
		int numale1 = 0, numale2 = 0, numale3 = 0;

		System.out.println("Ejercicio 8 Numero Aleatoreo");
		
		do {
			numale1 = (int) (Math.random() * 1000+1);
			numale2 = (int) (Math.random() * 1000+1);
			numale3 = (int) (Math.random() * 1000+1);

			int val1 = numale1 % 2;
			int val2 = numale2 % 2;
			int val3 = numale3 % 2;

			numensayo = numensayo + 1;

			if ((val1 == 0) && (val2 == 0) && (val3 > 0)) {
				listo = 1;
			}

		} while (listo != 1);

		System.out.println("**** Resultado ****");
		System.out.println("--> Numero 1: " + numale1);
		System.out.println("--> Numero 2: " + numale2);
		System.out.println("--> Numero 3: " + numale3);
		System.out.println("--> Numero de ensayos: " + numensayo);

	}

}
